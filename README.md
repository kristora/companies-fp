<!-- **Note**: If you want to update the README file, please make a
pull request and link to source (preferable the company itself) -->

Companies in Denmark using FP
=============================

* [DBC](#dbc)
* [Relink](#relink)
* [GoMore](#go-more)
* [Motosumu](#motosumu)
* [Shopgun](#shopgun)
* [Paqle](#paqle)
* [Isuu](#isuu)
* [Dixa](#dixa)
* [Sabre CMS](#sabre-cms)
* [Skatteministeriet](#skatteministeriet)
* [Prolog Development Center](#prolog-development-center)
* [Simcorp](#simcorp)
* [Deon Digital](#deon-digital)
* [Motorola Solutions](#motorola-solutions)
* [Unity](#unity)
* [Shape](#shape)
* [Hyperfactors](#hyperfactors)
* [Bilagscan](#bilagscan)
* [Zendesk](#zendesk)

DBC A/S [#dbc]
---
Has a team working with [NixOS](https://nixos.org)

Relink {#relink}
---
Relink (scala but also Node.js)


GoMore {#go-more}
---
goMore (clojure)


Motosumu {#motosumu}
---
motosumo (elixir)


Shopgun {#shopgun}
---
Shopgun - Erlang (This I will consider a functional programming oriented company)


Paqle {#paqle}
---
www.paqle.com/	Paqle A/S - Scala (www.paqle.com/)


Isuu {#isuu}
---
Issuu (Some OCaml and Erlang, mostly Python)


Dixa {#dixa}
---
Dixa - (Scala Akka)


Sabre CMS {#sabre-cms}
---
Sabre CMS (Tons of Clojure and ClojureScript)


Skatteministeriet {#skatteministeriet}
---
Skatteministeriet - (one team using Clojure)


Prolog Development Center {#prolog-development-center}
---
Prolog Development Center - PDC (visual prolog)


Simcorp {#simcorp}
---
Simcorp - (a little bit of OCaml. They have a tiny team of 10 mostly OCaml developers where they also have to know C# and some APL. Not functional programming  oriented where the most functional programming position is expecting to master one of : C#, OCaml and APL and be proficient in two of them)


Deon Digital {#deon-digital}
---
Deon Digital (Haskell and Kotlin, ñam!)


Motorola Solutions {#motorola-solutions}
---
Motorola Solutions (one team is using Elixir)


Unity {#unity}
---
Unity? in CDS


Shape {#shape}
---
Shape in some extent (probably very little, Erlang and Elixir)


Hyperfactors {#hyperfactors}
---
Hyperfactors (Scala an some probabilistic programming)


Bilagscan {#bilagscan}
---
bilagscan - the future of programming (probabilistic programming)


Zendesk {#zendesk}
---
is zendesk using ReasonML?
